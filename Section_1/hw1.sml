(* Homework1 *)
fun is_older(A : int * int * int, B : int * int * int) =
    ((#1 A) < (#1 B)) orelse
    (((#1 A) = (#1 B)) andalso ((#2 A) < (#2 B))) orelse
    (((#1 A) = (#1 B)) andalso ((#2 A) = (#2 B)) andalso ((#3 A) < (#3 B)))


fun number_in_month(L : (int * int * int) list, DesMonth: int) =    
    List.foldl (fn (X : int * int * int, Sum) =>
		   if (#2 X) = DesMonth then Sum + 1 else Sum) 0 L

fun number_in_months(L : (int * int * int) list, M : int list) =
    List.foldl (fn (DesMonth : int, Sum) =>
		   Sum + number_in_month(L, DesMonth)) 0 M

fun dates_in_month(L : (int * int * int) list, DesMonth: int) =
    if null L 
    then []
    else
	if (#2 (hd L) = DesMonth)
	then hd L :: dates_in_month(tl L, DesMonth)
	else dates_in_month(tl L, DesMonth)

fun dates_in_months(L : (int * int * int) list, M : int list)=
    if null M 
    then []
    else
	dates_in_month(L, hd M) @ dates_in_months(L, tl M)

fun get_nth(List, Nth : int)=
    List.nth(List, Nth-1)

fun date_to_string(Date : int * int * int)=
    let 
	val MonthNameList = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
    in
	get_nth(MonthNameList,#2 Date)^" "^Int.toString(#3 Date)^", "^Int.toString(#1 Date)
    end

fun number_before_reaching_sum(Sum : int, L : int list) =
    let 
	fun inner_help_func(L, AccSum, Pos) =
	    if null L
	    then Pos
	    else 
		if (hd L) + AccSum >= Sum
		then Pos-1
		else inner_help_func(tl L, (hd L) + AccSum, Pos+1)
		    
    in
	inner_help_func(L, 0, 1)
    end
		   

fun what_month(TotalDay : int ) =
    let
	val Days = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
    in
	1+number_before_reaching_sum(TotalDay, Days)
    end

fun month_range(Begin : int, End : int) =
    let
	val DaysSum = [31,59,90,120,151,181,212,243,273,304,334,365]
	fun inner_help_func(NewBegin : int, CurMonth : int, AccDay : int list) =
	    if NewBegin > End 
	    then []
	    else 
		if (hd AccDay) >= NewBegin
		then CurMonth::inner_help_func(NewBegin+1, CurMonth, AccDay)
		else 
		    inner_help_func(NewBegin, 1+CurMonth, tl AccDay)
    in
	inner_help_func(Begin, 1, DaysSum)
    end


fun oldest (L : (int * int * int) list) =
    if null L
    then NONE
    else let
	    fun max_nonempty (L : (int * int * int) list) =
		if null (tl L) 
		then hd L
		else let val Ans = max_nonempty(tl L)
		     in
			 if is_older(hd L, Ans)
			 then hd L
			 else Ans
		     end
	in
	    SOME (max_nonempty L)
	end

fun unique(L : int list) =
    let 
	fun inner_exists(L : int list, Num : int)=
	    List.exists (fn (X : int) =>
			    X = Num) L
    in
	List.foldl (fn(X : int, Acc : int list) =>
		       if inner_exists(Acc, X) 
		       then Acc
		       else Acc @ [X]) [] L
    end


fun number_in_months_challenge(L : (int * int * int) list, M : int list) =
    number_in_months(L, unique(M))

fun dates_in_months_challenge(L : (int * int * int) list,  M : int list) =
    dates_in_months(L, unique(M))


fun reasonable_date(Date : (int * int * int)) = 
    let 
	val Days =
	    if ((#1 Date) mod 400 = 0) orelse
	       ((#1 Date) mod 100 <> 0 andalso (#1 Date) mod 4 = 0)
	    then [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
	    else [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]


    in
	if (#1 Date) > 0 andalso ((#2 Date) >= 1 andalso (#2 Date) <= 12)
	then
	    if get_nth(Days, (#2 Date)) >= (#3 Date)
	    then true
	    else false
	else
	    false
    end
